# frozen_string_literal: true

$LOAD_PATH.unshift("#{File.dirname(__FILE__)}/lib")
require 'custom_renderer'

activate :autoprefixer do |prefix|
  prefix.browsers = 'last 2 versions'
end

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

activate :syntax

configure :build do
  set :build_dir, 'public'

  activate :minify_css
  activate :minify_javascript
  activate :relative_assets
end

set :markdown,
    renderer: CustomRenderer,
    strikethrough: true,
    fenced_code_blocks: true,
    smartypants: true,
    autolink: true
set :markdown_engine, :redcarpet

activate :blog do |blog|
  blog.name = 'blog'
  blog.prefix = 'blog'
  blog.paginate = true
  blog.per_page = 20
end
