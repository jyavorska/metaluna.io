# frozen_string_literal: true

require 'middleman-core/renderers/redcarpet'

class CustomRenderer < Middleman::Renderers::MiddlemanRedcarpetHTML
  def header(text, header_level)
    format('<h%s id="%s">%s</h%s>', header_level, text.parameterize, text, header_level)
  end

  def list_item(text, _list_type)
    if text.start_with?('[x]', '[X]')
      text[0..2] = %(<input type="checkbox" checked="checked" disabled>)
    elsif text.start_with?('[ ]')
      text[0..2] = %(<input type="checkbox" disabled>)
    end

    %(<li>#{text}</li>)
  end
end
