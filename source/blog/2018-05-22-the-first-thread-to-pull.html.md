---
title: "The First Thread to Pull when Learning Something New"
date: "2018-05-22"
category: general
description: What I've found to be the most effective way to just dive in and learn something.
layout: layout_blog
---

Learning something new is always an interesting challenge; the process of sitting down and getting started is always a bit daunting and, at least for me, figuring out where to pull the first thread is never quite clear. Lately I've been picking up a number of new hobbies and interests: I've started learning piano, decided to finally take getting better at chess seriously, I'm studying Dutch since I moved to the Netherlands recently, and am also learning the Kubernetes and OpenShift container platforms for work.

I used to be the kind of person who worried a lot about how to maximize my time studying - in particular I did this alot as I was getting started with learning the Dutch language. Should I be doing vocabulary flashcards? Grammar drills? Conversations with people? How can I find materials that aren't too challenging, but also not too easy? Considering all of these elements gave me a sort of "analysis paralysis" that in some cases even prevented me from studying while I searched for the most optimal way to study.

What I've found, however, was that the most important thing for me to actually engage my learning was to enter a sort of stressful, performant situation with it. Hiding away and honing my skills in secret did result in some progress of course, but in every case since I've realized this insight, the act of performing in front of a friendly audience has accelerated my rate of learning to the next gear.

This realization is actually backed up by neurobiology research. [This study](http://gsi.berkeley.edu/gsi-guide-contents/learning-theory-research/neuroscience/), for example, identified a few key learning principles that directly my experience - to directly quote that article:

- From the point of view of neurobiology, learning involves changing the brain.
- Moderate stress is beneficial for learning, while mild and extreme stress are detrimental to learning.
- Adequate sleep, nutrition, and exercise encourage robust learning.
- Active learning takes advantage of processes that stimulate multiple neural connections in the brain and promote memory.

So what does this mean? Well, my advice to you if you're learning something is to start showcasing your ability in front of your friends and family well before you think you're ready. Don't play chess against AI opponents as you seek to improve, telling yourself that you'll switch to real people when you get to a certain basic capability level, dive in right away. Go to language meetups where you speak broken grammar, but manage to get your point across. Play piano for your friends when they visit, even if you can't quite get it right. Just set up your Kubernetes environment or start coding in your new programming language when you have no idea what you're doing.

You'll make mistakes, sure, but those mistakes you make will be much more vivid and catalyze your learning so much more with just that key bit of intensity that comes from really engaging with real humans as you learn. Plus, the fun of social interaction makes any potential embarassment coming from the mistakes you make more tolerable. So get out there, join a local activity group for your new hobby, and see how fast your skills grow!
