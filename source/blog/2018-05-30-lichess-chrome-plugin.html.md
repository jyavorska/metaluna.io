---
title: "Replacing the Chrome 'new' tab with something more fun"
date: "2018-05-30"
category: general
description: An extension that shows various chess inspiration quotes.
layout: layout_blog
---

I never really liked the Chrome "new" tab very much.. it shows a Google search bar (which is redundant to the one in the browser), and my most visited sites which generally tend to be tabs that are already open. I thought it would be cool to replace it with something more interesting, so the first thing I found was an extension that shows various inspiration quotes. Fun, but I found some of them were a bit not safe for work, and why miss an opportunity to learn something and write your own, so that's just what I did!

I play a bit of chess online, and two of the features that I like the most on the site are the often-funny chess quotes and the puzzle of the day. I decided that for my own "new" tab, I'd show both of these. Because Lichess is open source, I was able to find the quotes in the Lichess [GitHub repo](https://github.com/ornicar/lila/blob/master/modules/quote/src/main/Quote.scala). The Lichess [Developers Page](https://lichess.org/developers) also has instructions on how to embed the puzzle of the day on any site. The [Chrome developer documentation site](https://developer.chrome.com/extensions/override) had the last bit of info on how to create an extension that can override one of the default Chrome pages.

With this information, I had everything I need to get started. Nothing is ever quite as simple as you like, though, and this project was no exception. The first task wasn't so bad, convering the Scala quotes declaration into a JavaScript array. I didn't know JavaScript going into this project but it's a simple language and I was able to figure things out as I went.

What was more challenging, though, was figuring out the Chrome security rules about embedded JavaScript on a page. It's very particular about what is allowed, and getting content from an external site such as Lichess is considered an insecure operation and very tightly controlled. Figuring the complexities of this out was a fun challenge, though, and finding the Chrome [Content Security Policy page](https://developer.chrome.com/apps/contentSecurityPolicy) for extensions took me the rest of the way there.

If you're interested in writing your own new tab extension to replace the content with something more interesting to you, then I'd recommend giving it a shot! I was able to do it without really knowing JavaScript, and learned a lot in the process. I'm sure what I've done can be improved in many ways; I feel like there must be a better scalable version for the replacement of the iFrame to the Lichess puzzle of the day, and also thought that it might be nice to just cache the puzzle of the day since it by definition doesn't change on every reload. I also felt like using document.write to replace the content of the body element was surely not the most elegant way to do this.

You can find my source at [GitLab](https://gitlab.com/jyavorska/newtab-lichess), and the extension itself is available on the [Chrome Web Store](https://chrome.google.com/webstore/detail/lichess-new-tab/dboknomlhdmhmfoonfoibiakaeclfjna).
