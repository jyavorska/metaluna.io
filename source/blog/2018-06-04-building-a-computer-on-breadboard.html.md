---
title: "Building an 8-bit computer on breadboard"
date: "2018-06-04"
category: general
description: An amazing video series from Ben Eater that teaches you how to build a computer from scratch.
layout: layout_blog
---

Yesterday I found a great YouTube series from [Ben Eater](https://www.youtube.com/channel/UCS0N5baNlQWJCUrhCEo8WlA) that builds a simple 8-bit computer from electronics, doing an amazing job of explaining all the important principles along the way. Click below for the introduction video, or [here](https://www.youtube.com/watch?v=HyznrdDSSGM&list=PLowKtXNTBypGqImE405J2565dvjafglHU) to jump to the whole playlist.

[![Building an 8-bit breadboard computer!](https://img.youtube.com/vi/HyznrdDSSGM/0.jpg)](https://www.youtube.com/watch?v=HyznrdDSSGM)

I've always wanted to build a computer like this to learn about the fundamentals, and this video series has been the next best thing; I stayed up a bit too late last night binge watching the first two thirds of the entire series. Ben really does a great job keeping things interesting and teaching a lot along the way, without assuming a lot of in-depth technical knowledge in advance (but also without seeming to ever spend a lot of time explaining anything obvious.)

If you've ever had an interest in electronics, I defninitely recommend checking it out. Maybe one day I'll try to build my own and track the progress here.
