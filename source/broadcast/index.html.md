---
title: "Learning Programming"
description: "My musings and links about teaching children to code."
---

Growing up my dad worked as a radio DJ, so I spent a lot of time in the late 80s and early 90s in
radio stations. I love seeing the old equipment and hearing the radio from this time, so I am
collecting here any interesting resourcese I find. I'm particularly interested in unedited
recordings of radio from this time that include commercials, music, and DJs; if you have any to
share please reach out.

### Equipment

- [Vintage radio equipment archive](https://www.oldradio.com/archives/hardware/)

### Airchecks

- [1988 recording of my father doing a prank on his first day at a new station](../assets/KXIQ_FM_Maniac_Lenny_Aug_16_1989.mp3) (no music, commercials)
