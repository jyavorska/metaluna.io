---
title: "DVDs"
description: "My collection of DVDs."
---

## Television

- Bonanza Collector's Box
- Eastbound and Down Seasons 1-4
- Peep Show Complete Box Set
- Penoza II
- Silicon Valley Seasons 1-6
- Office (UK), The Series 1
- Westworld Series 1
- Workaholics Complete Series

## Films

- 40-Year Old Virgin, The
- Almost Heroes
- Annie Hall
- Arrival
- Christmas Story, A
- Christmas Story 2, A
- Abyss
- Anything Else (Woody Allen Collection)
- Attack of the 50 Foot Woman
- Austin Powers
- Austin Powers: The Spy Who Shagged Me
- Back to the Future
- Bad Lieutenant: Port of Call New Orleans
- Barton Fink
- Basta Che Funzioni (Woody Allen Collection)
- Battlefield Earth
- Beetlejuice
- Blades of Glory
- Blues Brothers
- Burnt by the Sun
- Captain Fantastic
- Christmas Vacation
- Cloverfield
- Clue
- Dawn of the Dead
- Day the Earth Stood Still, The
- Doctor Zhivago
- Dumb and Dumber
- Dummy
- Dune
- Ed Wood
- European Vacation
- Everything Everywhere All at Once
- Evil Dead Trilogy
- Fear and Loathing in Las Vegas
- Fiddler on the Roof
- Fight Club
- Fist of Fear (Bruce Lee)
- Galaxy Quest
- Gladiator
- Godfather, The
- Gone with the Wind
- Goonies, The
- Gremlins
- Hangover, The
- Heat
- Heathers
- Highlander
- Hitchhiker's Guide to the Galaxy, The
- Hollywood Ending (Woody Allen Collection)
- Hot Fuzz
- I <3 Huckabees
- Indiana Jones and the Last Crusade
- Indiana Jones and the Raiders of the Lost Ark
- Indiana Jones and the Temple of Doom
- Interview with the Vampire
- Jerk, The
- Jingle All the Way
- Julie & Julia
- Jurassic Park
- Jurassic Park II
- Jurassic Park: The Lost World
- Kill Bill Volume 1
- King's Speech, The
- Kingpin
- Le Tango des Rashevski
- Lord of the Rings Trilogy
- Lost Highway
- Man of la Mancha
- Match Point (Woody Allen Collection)
- Matrix, The
- Maximum Overdrive
- Mars Attacks
- Martian, The
- Meet the Parents
- Midnight in Paris
- Midnight in Paris (Woody Allen Collection)
- Mighty Aphrodite
- Monty Python's Life of Brian
- Monty Python's Meaning of Life
- Mulholland Drive
- Old School
- Pacific Rim
- Pineapple Express
- Planes, Trains, and Automobiles
- Producers, The
- Purple Rose of Cairo, The
- Rambo Trilogy
- Red Scorpion
- Return of the Killer Tomatoes
- Rocky Horror Picture Show, The
- Romeo + Juliet
- Room, The
- Scarface
- School of Rock
- Scoop (Woody Allen Collection)
- Secret Life of Walter Mitty, The
- Shining, The
- Sing Street
- Singin' in the Rain
- Space Truckers
- Starship Troopers
- Step Brothers
- Strangers on a Train
- Tales from the Darkside
- They Live
- Thing, The
- This is Spinal Tap
- Three Amigos
- Tinker Tailor Soldier Spy
- Titanic
- To Rome with Love (Woody Allen Collection)
- Touch of Death (Bruce Lee)
- Trailer Park Boys, The Movie
- Tropic Thunder
- True Grit
- Unforgiven
- Vacation
- Vegas Vacation
- Vicky Cristina Barcelona (Woody Allen Collection)
- Walk Hard: The Dewey Cox Story
- Wild
- Young Frankenstein
- Zoolander

## Kids

### Television

- Beste van Bumba, Het
- Buurman & Buurman
- Little Rascals in Color
- Nijntje en Boris
- Nijntje en Knorretje
- Nijntje en Snuffie
- Nijntje en Vrienden
- Peppa Pig: Pannekoeken
- Sesaamstraat Seizoenen
- Sesaamstraat: Spelletjes en Knutselen
- Lone Ranger Volume 2, The
- Woezel en Pip Animatieserie Deel 3
- Woezel en Pip Verzamelbox

### Films

- American Tail, An
- Arrietty the Borrower
- Bambi
- Cat Returns, The
- Finding Nemo
- Harry Potter en de Vuurbeker
- Howl's Moving Castle
- Kiki's Delivery Service
- Lion King
- My Neighbor Totoro
- Nijntje the Film
- Ponyo
- Secret World of Arrietty, The
- Spirited Away
- Winne the Poeh: Een Gelukkig Poeh-Jaar

### Documentaries

- 24/7 Wild Earth Live
