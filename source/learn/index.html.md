---
title: "Learning Programming"
description: "My musings and links about teaching children to code."
---

_I'd love to collaborate with anyone on either writing books or building projects related to this topic! Please reach out if interested._

I was very lucky as a boy to get a [Commodore VIC-20](https://en.wikipedia.org/wiki/Commodore_VIC-20) computer around my sixth birthday; using it I was able to learn BASIC by typing in the programs in various books or magazines (the one I remember most fondly was [Gortek and the Microchips](https://archive.org/details/Gortek_and_the_Microchips_1984_Commodore).) I went on from there to [GW-BASIC](https://en.wikipedia.org/wiki/GW-BASIC), [QBasic](https://en.wikipedia.org/wiki/QBasic) (and the compiled version in [QuickBASIC](https://en.wikipedia.org/wiki/QuickBASIC), then [Visual Basic](https://en.wikipedia.org/wiki/Visual_Basic). By that point I was around fifteen and learned the language that I started my career in Ops automation with: [Perl](https://en.wikipedia.org/wiki/Perl).

Without that first experience with my VIC-20, it's hard to say what I'd be doing instead. Now that I have a five year old daughter myself, I'm using this place to collect links and resources to help her learn too. If you have any suggestions to add here, please let me know.

### REPLs/Fantasy Consoles

- [PICO-8 tiny (virtual) computer for learning games](https://www.lexaloffle.com/pico-8.php)
- [BASIC editor and runtime on repl.it](https://repl.it/l/basic)
- [Applesoft BASIC in JavaScript](https://www.calormen.com/jsbasic/)
- [QB64 BASIC compiler for Win, Mac, Linux](https://www.qb64.org/portal/)
- [Various emulators written in JavaScript](https://www.cambus.net/emulators-written-in-javascript/)
- [SmileBASIC](http://smilebasic.com/)
- [JS Turtle Art](https://turtle.sugarlabs.org/)
- [Turtle graphics in JavaScript](https://github.com/CodeGuppyPrograms/TurtleGFX)
- [24a2 minimal game engine](https://github.com/jamesroutley/24a2)

### Learning Environments

- [MIT Scratch](https://scratch.mit.edu/)
- [Microsoft MakeCode](https://www.microsoft.com/en-us/makecode)
- [CodeCombat](https://codecombat.com/)

### Program Listings

- [BASIC computer games book](https://www.atariarchives.org/basicgames/)
- [Gortek and the Microchips book](https://archive.org/details/Gortek_and_the_Microchips_1984_Commodore)

### Curricula/Indexes

- [arkmsworld's list of programming resources (incl. BASIC)](https://arkmsworld.neocities.org/programming.html)
- [Curated list of fantasy consoles/computers](https://github.com/paladin-t/fantasy)
- [Computer science teaching resources](http://users.csc.calpoly.edu/~zwood/Outreach/)
